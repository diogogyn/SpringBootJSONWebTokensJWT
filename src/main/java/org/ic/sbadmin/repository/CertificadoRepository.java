package org.ic.sbadmin.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.ic.sbadmin.model.Certificado;
import org.springframework.stereotype.Repository;

/**
 * 
 *          CLASSE QUE VAI REALIZAR A PERSISTeNCIA DO NOSSO OBJETO ProfissionalModel
 *          NO BANCO DE DADOS.
 * 
 * 
 */
@Repository
public class CertificadoRepository {

	/**
	 * @PersistenceContext é o local onde ficam armazenados as entidades que
	 *                     estao sendo manipuladas pelo EntityManager
	 * 
	 * 
	 * @PersistenceContext(type = PersistenceContextType.EXTENDED) assim o
	 *                          servidor vai gerenciar para nos as entidades.
	 * 
	 **/
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager manager;

	/**
	 * 
	 * @param viaModel
	 * 
	 *            Salva um novo registro
	 * 
	 *            O JPA exige um contexto de transacao para realizar as
	 *            alteracoes, por isso vamos usar a
	 *            anotacao @javax.transaction.Transactional
	 * 
	 */
	@javax.transaction.Transactional
	public void Salvar(Certificado viaModel) {

		manager.persist(viaModel);
	}

	/**
	 * 
	 * @param idCertificado
	 * @return Certificado
	 * 
	 *         Consulta uma via pelo ID
	 */
	public Certificado ConsultarPorCodigo(int idCertificado) {

		return manager.find(Certificado.class, idCertificado);
	}

	/**
	 * 
	 * @param idCertificado
	 * 
	 *            Exclui uma via por ID
	 */
	@javax.transaction.Transactional
	public void Excluir(int idCertificado) {

		Certificado viaModel = this.ConsultarPorCodigo(idCertificado);

		manager.remove(viaModel);

	}

	/**
	 * 
	 * @return List<Profissional>
	 * 
	 *         Consulta todos os profissionais no banco de
	 *         dados
	 */
	public List<Certificado> TodosCertificados() {

		return manager.createQuery("SELECT c FROM Certificado c ORDER BY c.nome ", Certificado.class).getResultList();
	}

}
