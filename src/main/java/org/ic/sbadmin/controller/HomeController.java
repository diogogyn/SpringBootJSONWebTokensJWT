package org.ic.sbadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
		return "index2";
	}

	@RequestMapping("/index")
	public String index() {
		return "home/home";
	}
	
	/*
	 *  @RequestMapping => value  => Defini o caminho para a chamada da view. 
	 * @RequestMapping => method => Defini o o m�todo http que o m�todo vai responder.
	 */
	@RequestMapping("/listaProfissionais")
	public String listaProfissionais() {
		return "profissional/listaProfissionais";
	}

	@RequestMapping("/cadastroProfissional")
	public String cadastroProfissional() {
		return "profissional/cadastroProfissional";
	}

	@RequestMapping("/gerenciaPostoFiscalizacao")
	public String gerenciaPostoFiscalizacao() {
		return "postos/gerenciaPostoFiscalizacao";
	}

	@RequestMapping("/cadastroPostoFiscalizacao")
	public String cadastroPostoFiscalizacao() {
		return "postos/cadastroPostoFiscalizacao";
	}

	@RequestMapping("/listarPostosFiscalizacao")
	public String listarPostosFiscalizacao() {
		System.out.println("passou controler listaPostoFiscalizacao");
		return "postos/listarPostosFiscalizacao";
	}

	@RequestMapping("/cadastroVias")
	public String cadastroVias() {
		return "vias/cadastroVias";
	}

	@RequestMapping("/listaVias")
	public String listaVias() {
		return "vias/listaVias";
	}
	
	@RequestMapping("/registroAutuacao")
	public String registroAutuacao() {
		return "relatorios/registroAutuacao";
	}
	
	@RequestMapping("/autuacoesPorPostoFiscalizacao")
	public String autuacoesPorPostoFiscalizacao() {
		return "relatorios/autuacoesPorPostoFiscalizacao";
	}
	
	@RequestMapping("/fluxoPorPostoFiscalizacao")
	public String fluxoPorPostoFiscalizacao() {
		return "relatorios/fluxoPorPostoFiscalizacao";
	}
	//autuacoesPorPostoFiscalizacao
}
