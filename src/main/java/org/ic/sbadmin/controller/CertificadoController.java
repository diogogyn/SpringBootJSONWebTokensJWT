package org.ic.sbadmin.controller;

import java.util.List;

import org.ic.sbadmin.model.Certificado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.ic.sbadmin.model.ResultadoModel;
import org.ic.sbadmin.repository.CertificadoRepository;

public class CertificadoController {
	/**
	 * @Autowired => injetando o objeto resultadoModel no nosso controller
	 */
	@Autowired
	ResultadoModel resultadoModel;

	/**
	 * Injetando o objeto certificadoRepository
	 */
	@Autowired
	CertificadoRepository certificadoRepository;

	/**
	 * Chama a view (editarRegistro.jsp) para editar um registro cadastrado
	 * 
	 */
	@RequestMapping(value = "/consultaCertificado/{idCertificado}", method = RequestMethod.GET)
	public ModelAndView GerenciarPosto(@PathVariable int idCertificado) {

		Certificado certificadoModel = certificadoRepository.ConsultarPorCodigo(idCertificado);

		return new ModelAndView("gerenciaProfisisonal", "certificadoModel", certificadoModel);
	}

	/**
	 * 
	 * Salva um novo registro via ajax, esse metodo vai ser chamado pelo
	 * cadastraoCertificadoCtrl.js atraves do AngularJS
	 * 
	 */
	@RequestMapping(value = "/enviarCertificado", method = RequestMethod.POST)
	public @ResponseBody ResultadoModel Salvar(@RequestBody Certificado certificadoModel) {

		try {

			certificadoRepository.Salvar(certificadoModel);

			resultadoModel.setCodigo(1);
			resultadoModel.setMensagem("Certificado cadastrado com sucesso!");

		} catch (Exception e) {

			resultadoModel.setCodigo(2);
			resultadoModel.setMensagem("Erro ao salvar o certificado (" + e.getMessage() + ")");
		}

		return resultadoModel;
	}

	/**
	 * Consulta todos os registros cadastrados
	 * 
	 */
	@RequestMapping(value = "/consultarTodosCertificados", method = RequestMethod.GET)
	public @ResponseBody List<Certificado> ConsultarTodos() {

		return certificadoRepository.TodosCertificados();// trocar
	}

	/**
	 * Excluir um certificado pelo codigo
	 * 
	 */
	@RequestMapping(value = "/excluirCertificado/{idCertificado}", method = RequestMethod.DELETE)
	public @ResponseBody void ExcluirRegistro(@PathVariable int idCertificado) {

		certificadoRepository.Excluir(idCertificado);
	}

}
