package org.ic.sbadmin.model.dominio;

public enum TipoVia {
	
	RUA, AVENIDA, ESTRADA, EXPRESSA;

}
