package org.ic.sbadmin.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the certificado database table.
 * 
 */
@Entity
@Table(name="certificado")
@NamedQuery(name="Certificado.findAll", query="SELECT p FROM Certificado p")
public class Certificado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int idProfissional;

	@Column(name="nome")
	private String nome;

	@Column(name="certificado")
	private String certificado;

	public Certificado() {
	}

	
	public int getIdProfissional() {
		return idProfissional;
	}


	public void setIdProfissional(int idProfissional) {
		this.idProfissional = idProfissional;
	}


	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCertificado() {
		return certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
 
}