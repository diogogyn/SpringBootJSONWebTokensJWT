package org.ic.sbadmin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the autuacoes database table.
 * 
 */
@Entity
@Table(name="autuacoes")
@NamedQuery(name="Autuacoes.findAll", query="SELECT a FROM Autuacoes a")
public class Autuacoes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_autuacao")
	private int idAutuacao;

	@Column(name="codigo_rfid")
	private String codigoRfid;

	@Temporal(TemporalType.DATE)
	@Column(name="data_infracao")
	private Date dataInfracao;

	@Column(name="vel_aferida")
	private int velAferida;

	@Column(name="vel_considerada")
	private int velConsiderada;

	//bi-directional many-to-one association to PostoFiscalizacao
	@ManyToOne
	@JoinColumn(name="posto_fiscalizacao_id_posto")
	private PostoFiscalizacao postoFiscalizacao;

	//bi-directional many-to-one association to Veiculo
	@ManyToOne
	private Veiculo veiculo;

	public Autuacoes() {
	}

	public int getIdAutuacao() {
		return this.idAutuacao;
	}

	public void setIdAutuacao(int idAutuacao) {
		this.idAutuacao = idAutuacao;
	}

	public String getCodigoRfid() {
		return this.codigoRfid;
	}

	public void setCodigoRfid(String codigoRfid) {
		this.codigoRfid = codigoRfid;
	}

	public Date getDataInfracao() {
		return this.dataInfracao;
	}

	public void setDataInfracao(Date dataInfracao) {
		this.dataInfracao = dataInfracao;
	}

	public int getVelAferida() {
		return this.velAferida;
	}

	public void setVelAferida(int velAferida) {
		this.velAferida = velAferida;
	}

	public int getVelConsiderada() {
		return this.velConsiderada;
	}

	public void setVelConsiderada(int velConsiderada) {
		this.velConsiderada = velConsiderada;
	}

	public PostoFiscalizacao getPostoFiscalizacao() {
		return this.postoFiscalizacao;
	}

	public void setPostoFiscalizacao(PostoFiscalizacao postoFiscalizacao) {
		this.postoFiscalizacao = postoFiscalizacao;
	}

	public Veiculo getVeiculo() {
		return this.veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

}