package org.ic.sbadmin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the profissional database table.
 * 
 */
@Entity
@Table(name="profissional")
@NamedQuery(name="Profissional.findAll", query="SELECT p FROM Profissional p")
public class Profissional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_profissional")
	private int idProfissional;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cadastro")
	private Date dataCadastro;
	
	@Column(name="funcao")
	private String funcao;
	
	@Column(name="login")
	private String login;

	@Column(name="nome")
	private String nome;

	@Column(name="senha")
	private String senha;

	public Profissional() {
	}

	public int getIdProfissional() {
		return this.idProfissional;
	}

	public void setIdProfissional(int idProfissional) {
		this.idProfissional = idProfissional;
	}

	public Date getDataCadastro() {
		return this.dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getFuncao() {
		return this.funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}