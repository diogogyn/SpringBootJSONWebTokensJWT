package org.ic.sbadmin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the posto_fiscalizacao database table.
 * 
 */
@Entity
@Table(name="posto_fiscalizacao")
@NamedQuery(name="PostoFiscalizacao.findAll", query="SELECT p FROM PostoFiscalizacao p")
public class PostoFiscalizacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_posto")
	private int idPosto;

	@Column(name="distancia_antenas")
	private int distanciaAntenas;

	@Column(name="endereco_ip")
	private String enderecoIp;
	
	@Column(name="localizacao")
	private String localizacao;

	@Column(name="nome_posto")
	private String nomePosto;

	@Column(name="situacao_posto")
	private String situacaoPosto;

	//bi-directional many-to-one association to Autuacoe
	@OneToMany(mappedBy="postoFiscalizacao")
	private List<Autuacoes> autuacoes;

	//bi-directional many-to-one association to Via
	@ManyToOne
	@JoinColumn(name="vias_id_via")
	private Via via;

	public PostoFiscalizacao() {
	}

	public int getIdPosto() {
		return this.idPosto;
	}

	public void setIdPosto(int idPosto) {
		this.idPosto = idPosto;
	}

	public int getDistanciaAntenas() {
		return this.distanciaAntenas;
	}

	public void setDistanciaAntenas(int distanciaAntenas) {
		this.distanciaAntenas = distanciaAntenas;
	}

	public String getEnderecoIp() {
		return this.enderecoIp;
	}

	public void setEnderecoIp(String enderecoIp) {
		this.enderecoIp = enderecoIp;
	}

	public String getLocalizacao() {
		return this.localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getNomePosto() {
		return this.nomePosto;
	}

	public void setNomePosto(String nomePosto) {
		this.nomePosto = nomePosto;
	}

	public String getSituacaoPosto() {
		return this.situacaoPosto;
	}

	public void setSituacaoPosto(String situacaoPosto) {
		this.situacaoPosto = situacaoPosto;
	}

	public List<Autuacoes> getAutuacoes() {
		return this.autuacoes;
	}

	public void setAutuacoes(List<Autuacoes> autuacoes) {
		this.autuacoes = autuacoes;
	}

	public Autuacoes addAutuacoe(Autuacoes autuacoe) {
		getAutuacoes().add(autuacoe);
		autuacoe.setPostoFiscalizacao(this);

		return autuacoe;
	}

	public Autuacoes removeAutuacoe(Autuacoes autuacoe) {
		getAutuacoes().remove(autuacoe);
		autuacoe.setPostoFiscalizacao(null);

		return autuacoe;
	}

	public Via getVia() {
		return this.via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

}