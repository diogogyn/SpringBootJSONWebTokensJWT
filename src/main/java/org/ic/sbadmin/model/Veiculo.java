package org.ic.sbadmin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the veiculo database table.
 * 
 */
@Entity
@Table(name="veiculo")
@NamedQuery(name="Veiculo.findAll", query="SELECT v FROM Veiculo v")
public class Veiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_veiculo")
	private int idVeiculo;

	@Column(name="codigo_rfid")
	private String codigoRfid;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cadastro")
	private Date dataCadastro;
	
	@Column(name="placa")
	private String placa;
	
	@Column(name="proprietario")
	private String proprietario;

	//bi-directional many-to-one association to Autuacoe
	@OneToMany(mappedBy="veiculo")
	private List<Autuacoes> autuacoes;

	public Veiculo() {
	}

	public int getIdVeiculo() {
		return this.idVeiculo;
	}

	public void setIdVeiculo(int idVeiculo) {
		this.idVeiculo = idVeiculo;
	}

	public String getCodigoRfid() {
		return this.codigoRfid;
	}

	public void setCodigoRfid(String codigoRfid) {
		this.codigoRfid = codigoRfid;
	}

	public Date getDataCadastro() {
		return this.dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getProprietario() {
		return this.proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public List<Autuacoes> getAutuacoes() {
		return this.autuacoes;
	}

	public void setAutuacoes(List<Autuacoes> autuacoes) {
		this.autuacoes = autuacoes;
	}

	public Autuacoes addAutuacoe(Autuacoes autuacoe) {
		getAutuacoes().add(autuacoe);
		autuacoe.setVeiculo(this);

		return autuacoe;
	}

	public Autuacoes removeAutuacoe(Autuacoes autuacoe) {
		getAutuacoes().remove(autuacoe);
		autuacoe.setVeiculo(null);

		return autuacoe;
	}

}