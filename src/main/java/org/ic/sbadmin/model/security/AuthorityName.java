package org.ic.sbadmin.model.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}