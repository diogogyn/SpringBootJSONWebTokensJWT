package org.ic.sbadmin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the vias database table.
 * 
 */
@Entity
@Table(name="vias")
@NamedQuery(name="Via.findAll", query="SELECT v FROM Via v")
public class Via implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_via")
	private int idVia;
	
	@Column(name="cep")
	private String cep;
	
	@Column(name="cidade")
	private String cidade;

	@Column(name="nome_via")
	private String nomeVia;

	@Column(name="tipo_via")
	private String tipoVia;

	@Column(name="uf")
	private String uf;

	@Column(name="vel_max")
	private int velMax;
	
	@Column(name="bairro")
	private String bairro;

	//bi-directional many-to-one association to PostoFiscalizacao
	@OneToMany(mappedBy="via")
	private List<PostoFiscalizacao> postoFiscalizacaos;

	public Via() {
	}

	public int getIdVia() {
		return this.idVia;
	}

	public void setIdVia(int idVia) {
		this.idVia = idVia;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getNomeVia() {
		return this.nomeVia;
	}

	public void setNomeVia(String nomeVia) {
		this.nomeVia = nomeVia;
	}

	public String getTipoVia() {
		return this.tipoVia;
	}

	public void setTipoVia(String tipoVia) {
		this.tipoVia = tipoVia;
	}

	public String getUf() {
		return this.uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public int getVelMax() {
		return this.velMax;
	}

	public void setVelMax(int velMax) {
		this.velMax = velMax;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public List<PostoFiscalizacao> getPostoFiscalizacaos() {
		return this.postoFiscalizacaos;
	}

	public void setPostoFiscalizacaos(List<PostoFiscalizacao> postoFiscalizacaos) {
		this.postoFiscalizacaos = postoFiscalizacaos;
	}

	public PostoFiscalizacao addPostoFiscalizacao(PostoFiscalizacao postoFiscalizacao) {
		getPostoFiscalizacaos().add(postoFiscalizacao);
		postoFiscalizacao.setVia(this);

		return postoFiscalizacao;
	}

	public PostoFiscalizacao removePostoFiscalizacao(PostoFiscalizacao postoFiscalizacao) {
		getPostoFiscalizacaos().remove(postoFiscalizacao);
		postoFiscalizacao.setVia(null);

		return postoFiscalizacao;
	}

}