package org.ic.sbadmin.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.ic.sbadmin.model.security.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
