<%@ tag description="Template Site Tag" language="java"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@attribute name="title" fragment="true"%>

<html lang="pt_br" ng-app="app">

<head>
<title><jsp:invoke fragment="title" /></title>
<!-- Bootstrap Core CSS -->
<spring:url value="/resources/css/bootstrap.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />

<!-- MetisMenu CSS -->
<spring:url value="/resources/css/metisMenu.min.css" var="metisMenuCss" />
<link href="${metisMenuCss}" rel="stylesheet">

<!-- Timeline CSS -->
<spring:url value="/resources/css/timeline.css" var="timeline" />
<link href="${timeline}" rel="stylesheet">

<!-- Custom CSS -->
<spring:url value="/resources/css/sb-admin-2.css" var="sbAdmin2Css" />
<link href="${sbAdmin2Css}" rel="stylesheet">

<!-- Custom Fonts -->
<spring:url value="/resources/css/font-awesome.min.css"
	var="fontAwesomeCss" />
<link href="${fontAwesomeCss}" rel="stylesheet">

<spring:url value="/resources/js/angular.min.js" var="angularJs" />
<script src="${angularJs}"></script>

<spring:url value="/resources/js/angular-resource.min.js"
	var="angularResourceJs" />
<script src="${angularResourceJs}"></script>

<spring:url value="/resources/js/angular-route.min.js"
	var="angularRouteJs" />
<script src="${angularRouteJs}"></script>

<!-- DataTables CSS -->
<spring:url value="/resources/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css"
	var="dataTablesBootstrapCss" />
<link href="${dataTablesBootstrapCss}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<spring:url value="/resources/bower_components/datatables-responsive/css/dataTables.responsive.css" var="dataTablesResponsiveCss" />
<link href="${dataTablesResponsiveCss}" rel="stylesheet">

<spring:url value="/resources/js/app/app.js" var="appJs" />
<script src="${appJs}"></script>

<spring:url value="/resources/js/app/configs/routerConfig.js"
	var="routerConfigJs" />
<script src="${routerConfigJs}"></script>

<spring:url value="/resources/js/app/controllers/indexCtrl.js"
	var="indexCtrl" />
<script src="${indexCtrl}"></script>

<spring:url
	value="/resources/js/jwt-decode.js"
	var="jwt-decod" />
<script src="${jwt-decod}"></script>
</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				
				<a class="navbar-brand" href="index.html">Aplicativo</a>
			</div>
			<!-- /.navbar-header -->


			<!-- MENU -->
			<div class="navbar-default sidebar" role="navigation">
				
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<div id="page-wrapper">
			
			<jsp:doBody />


		</div>
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<spring:url value="/resources/bower_components/jquery/dist/jquery.min.js" var="jqueryJs" />
	<script src="${jqueryJs}"></script>

	<!-- Bootstrap Core JavaScript -->
	<spring:url value="/resources/js/bootstrap.min.js" var="bootstrapJs" />
	<script src="${bootstrapJs}"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<spring:url value="/resources/js/metisMenu.min.js" var="metisMenuJs" />
	<script src="${metisMenuJs}"></script>

	<!-- Custom Theme JavaScript -->
	<spring:url value="/resources/js/sb-admin-2.js" var="sbAdmin2Js" />
	<script src="${sbAdmin2Js}"></script>
	
	<!-- DataTables JavaScript -->
    <spring:url value="/resources/bower_components/datatables/media/js/jquery.dataTables.min.js" var="dataTablesMinJs" />
	<spring:url value="/resources/bower_components/datatables/media/js/dataTables.bootstrap.min.js" var="dataTablesBootstrapJs" />
    <script src="${dataTablesMinJs}"></script>
    <script src="${dataTablesBootstrapJs}"></script>
	
</body>

</html>
