
app.config(function($routeProvider){
	
	console.log("passou por routConfig!!!");
	
	$routeProvider
		.when('/',{
			templateUrl: '/index',
			controller: 'indexCtrl'
		})
		.when("/cadastroProfissional",{
			templateUrl: '/cadastroProfissional',
			controller: 'novoProfissionalCtrl'
		})
		.when("/listaProfissionais",{
			templateUrl: '/listaProfissionais',
			controller: 'listaProfissionaisCtrl'
		})
		.when("/listarPostosFiscalizacao",{
			templateUrl: '/listarPostosFiscalizacao',
			controller: 'listarPostosFiscalizacaoCtrl'
		})
		.when("/cadastroPostoFiscalizacao",{
			templateUrl: '/cadastroPostoFiscalizacao',
			controller: 'cadastroPostoFiscalizacaoCtrl'
		})
		.when("/gerenciaPostoFiscalizacao",{
			templateUrl: '/gerenciaPostoFiscalizacao',
			controller: 'gerenciaPostoFiscalizacaoCtrl'
		})
		.when("/cadastroVias",{
			templateUrl: '/cadastroVias',
			controller: 'cadastroViasCtrl'
		})
		.when("/listaVias",{
			templateUrl: '/listaVias',
			controller: 'listaViasCtrl'
		})
		.when("/registroAutuacao",{
			templateUrl: '/registroAutuacao',
			controller: 'registroAutuacaoCtrl'
		})
		.when("/autuacoesPorPostoFiscalizacao",{
			templateUrl: '/autuacoesPorPostoFiscalizacao',
			controller: 'autuacoesPorPostoFiscalizacaoCtrl'
		})
		.when("/fluxoPorPostoFiscalizacao",{
			templateUrl: '/fluxoPorPostoFiscalizacao',
			controller: 'fluxoPorPostoFiscalizacaoCtrl'
		})
		//autuacoesPorPostoFiscalizacao
		.otherwise({redirectTo:'/'})
})